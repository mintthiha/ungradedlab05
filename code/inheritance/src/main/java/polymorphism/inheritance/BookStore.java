package polymorphism.inheritance;

public class BookStore {
  public static void main(String[] args) {
    Book[] books = new Book[5];
    books[0] = new Book("Harry Potter", "Amanda JK");
    books[1] = new ElectronicBook("Dance of Thieves", "JenRoxanne", 12341);
    books[2] = new Book("How to Drive", "Mintters");
    books[3] = new ElectronicBook("Verity", "Loyes Thea", 12301);
    books[4] = new ElectronicBook("Remember me", "Shadhe Omera", 43012);

    for (int i = 0; i < books.length; i++) {
      System.out.println(books[i]);
    }

    //books[0].getNumberBytes();
    //books[1].getNumberBytes();
    //ElectronicBook b = books[1];
    ElectronicBook b = (ElectronicBook)books[1];

    System.out.println(b.getNumberBytes());

    ElectronicBook c = (ElectronicBook)books[0];
    System.out.println(c.getNumberBytes());
  }
}
